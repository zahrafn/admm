#! /usr/bin/env python
import numpy as np
import math
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from math import sqrt, log
from numpy import linalg as LA
from itertools import combinations
from scipy.optimize import minimize
import time


def file_reader(filename ):

  inx = []
  x = []
  y = []
  z = []
  time = []

  position_file=open( filename , 'r')
  first_line = position_file.readline()

  for line in position_file:

    line=line.split(",")

    inx.append(np.int(line[0]))
    x.append(np.float(line[1]))
    y.append(np.float(line[2]))
    z.append(np.float(line[3]))
    time.append(np.float(line[4]))

  return [inx,x,y,z,time]


def main():
  plt.ion()


  #### reading each file
  gs = 'gs_posiotns_time.txt'
  admm = 'admm_posiotns_time.txt'
  euler = 'euler_posiotns_time.txt'

  # get all xs and ys
  inx_gs,x_gs,y_gs,z_gs,time_gs = file_reader(gs)
  inx_admm,x_admm,y_admm,z_admm,time_admm = file_reader(admm)
  inx_euler,x_euler,y_euler,z_euler,time_euler = file_reader(euler)

  uniqe_t_set = set(time_gs)
  uniqe_t_list = list(uniqe_t_set)
  num_frames = len(uniqe_t_set)

  uniqe_id_set = set(inx_gs)
  uniqe_id_list = list(uniqe_id_set)
  num_particles = len(uniqe_id_list)

  # print num_frames
  # print num_particles

  err_gs_list = []
  err_admm_list =[]
  for i in range(num_particles):

    ##### note Euler have step size of 0.001 
    #####      ADMM and GS have step size of 0.005 ( 5 * Euler )

    ########## get x position of each particle 
    x_gs_i = np.array(x_gs[i::num_particles])
    x_admm_i = np.array(x_admm[i::num_particles])
    x_euler_i = np.array(x_euler[i::num_particles*5])

    ########## get y position of each particle 
    y_gs_i = np.array(y_gs[i::num_particles])
    y_admm_i = np.array(y_admm[i::num_particles])
    y_euler_i = np.array(y_euler[i::num_particles*5])

    ########## get z position of each particle 
    z_gs_i = np.array(z_gs[i::num_particles])
    z_admm_i = np.array(z_admm[i::num_particles])
    z_euler_i = np.array(z_euler[i::num_particles*5])

    ########## find the error
    error_gs = np.sqrt( np.power((x_euler_i - x_gs_i), 2) + np.power((y_euler_i - y_gs_i), 2) +np.power((z_euler_i - z_gs_i), 2))
    error_admm = np.sqrt( np.power((x_euler_i - x_admm_i), 2) + np.power((y_euler_i - y_admm_i), 2) + np.power((z_euler_i - z_admm_i), 2))

    error_gs = error_gs.reshape(1,(len(error_gs)))
    error_admm = error_admm.reshape((1,len(error_admm)))

    # err_gs_list = np.append(err_gs_list, error_gs)
    # err_admm_list = np.append(err_admm_list, error_admm)

    if(i==0):
      err_gs_list = error_gs
      err_admm_list = error_admm
    else:
      err_gs_list =np.vstack(( err_gs_list, error_gs))
      err_admm_list =np.vstack(( err_admm_list, error_admm))


  #   print "error_gs \n", error_gs.shape
  # print "error_gs \n", err_gs_list.shape

  t = np.arange(num_frames)
  #plt.plot(t, err_gs_list[0], 'r--', t, err_admm_list[0], 'b-.')#, t, err_gs_list[2], 'g--', t, err_gs_list[3], 'b--' )
  plt.plot(t, err_gs_list[0], 'r--', t, err_gs_list[1], 'b-.', t, err_gs_list[2], 'g--', t, err_gs_list[3], 'b--' )

  plt.ylabel("err")
  plt.xlabel("time")
  plt.show()
  plt.pause(100)

  # t_step_num = np.array(num_frames)
  # plt.plot(t_step_num, err_gs_list[0], 'r--', t_step_num, err_gs_list[1], 'b-.', t_step_num, err_gs_list[2], 'g--', t_step_num, err_gs_list[3], 'b--' )
  # plt.ylabel("position")
  # plt.xlabel("time")
  # plt.show()
  # plt.pause(100)



  # x_gs_0 = [0::3]
  # x_gs_1 = [1::3]
  # x_gs_2 = [2::3]
  # x_gs_3 = [3::3]

if __name__ == "__main__":
    main()



