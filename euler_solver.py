#! /usr/bin/env python
import numpy as np
import math
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from math import sqrt, log
from numpy import linalg as LA
from itertools import combinations
from scipy.optimize import minimize
import time


class Spring:

    def __init__(self, s1, s2,inx, id1, id2, n, dim, stiffness):

        # the spring has always 2 masses holding each side of it
        self.s1 = s1
        self.s2 = s2
        self.id1 = id1
        self.id2 = id2
        self.inx = inx
        self.n =n
        self.dim = dim
        self.x = np.array([s1,s2])
        self.stiffness = stiffness
        self.weight = np.sqrt(self.stiffness)
        dist_v = self.s1-self.s2
        self.rest_dist = LA.norm(dist_v)

        
    
    def get_reduction_matrix(self, d_reduct):

        i_m = np.identity(self.dim)
        d_reduct[self.inx * self.dim : self.inx* self.dim + self.dim, self.id1*self.dim:self.id1*self.dim+self.dim] = -1 * i_m
        d_reduct[self.inx * self.dim : self.inx* self.dim + self.dim, self.id2*self.dim:self.id2*self.dim+self.dim] = i_m


        
        return d_reduct


#############################################################################################################################





def config_reader(filename ):
    x =[]
    v = []
    config=open( filename , 'r')
    first_line = config.readline()

    for line in config:

        line=line.split(",")
        if(line[0] != 'sets'):

            x.append(np.float(line[1]))
            x.append(np.float(line[2]))
            x.append(np.float(line[3]))
            v.append(np.float(line[4]))
            v.append(np.float(line[5]))
            v.append(np.float(line[6]))

        else:
            sets = np.array(line[1:])


    return x,v, sets





def main():

    ################# initialization #######################
    visualization = True
    out_put = True
    dim = 3
    filename = 'config.txt'

    ################# parameters 
    number_frames = 2000
    dt = 0.001
    init_mass = 1.
    stiffness = 10.
    ##########################################################

    ############# read position and velocity of particles from config file
    x,v, sets = config_reader(filename )
    x = np.array(x)
    v = np.array(v)

    number_particles = len(x)/ dim
    number_springs = len(sets)

    
    ################### making springs list

    springs =[]
    for i, s in enumerate(sets):

        s = s.strip()
        inx_1 = int(s[0])
        inx_2 = int(s[1])
        sp = Spring(x[inx_1 * dim : inx_1* dim + dim], x[inx_2 * dim : inx_2* dim + dim], i ,inx_1 , inx_2, number_particles ,dim, stiffness )
        springs = np.append(springs,sp)
    
    

    #going over all particles one by one 
    x = x.reshape((len(x), 1))
    v = v.reshape((len(v), 1))
    x_tilde = x
        
    
    print "################### simulation starts ###################"
    plt.ion()

    t = []
    t_all =0


    x_p = np.zeros(number_particles)
    y_p = np.zeros(number_particles) 
    z_p = np.zeros(number_particles) 


    if(out_put):

        print "############# writing out output file ####################"

        out=open('euler_posiotns_time.txt','w')
        out.write("id, x, y, z, time"+'\n')
        

        
        
    for f in range(number_frames):
            

        t  = np.append(t, t_all)

        if(out_put):
            for k in range(number_particles):
                out.write( ','.join([str(k), str(x[k*dim][0]), str(x[k*dim +1 ][0]), str(x[k*dim +2][0]), str(t[f]) ])+'\n')


    

        print "################### frame: ", f
        x_tilde = x 

        #while (error > target_error):
            
        energy_sum =0

            # for i in range(number_particles):

        forces = np.zeros((len(x),1))
        x =  x + v *dt

        for j, s in enumerate(springs):

            x_p1 = x[ s.id1*dim : s.id1*dim +dim] 
            x_p2 = x[ s.id2*dim : s.id2*dim +dim]
            dx = LA.norm(x_p1 -  x_p2)
            dx_dir = (x_p1 -  x_p2) / dx

            forces[s.id1*dim : s.id1*dim +dim] = forces[s.id1*dim : s.id1*dim +dim] + (s.stiffness * (s.rest_dist - dx)) * dx_dir
            forces[s.id2*dim : s.id2*dim +dim] = forces[s.id2*dim : s.id2*dim +dim] - (s.stiffness * (s.rest_dist - dx)) * dx_dir


        acceleration = forces/ init_mass
        v =  v + acceleration*dt

        t_all = t_all+dt


        if(f==0):
            snapshot_positions = x
        else:
            snapshot_positions =np.hstack(( snapshot_positions, x))


        ########## plot position x vs position y
        # for k in range(number_particles):



        #     x_p[k] = x[k*dim]
        #     y_p[k] = x[k*dim + 1]
        #     z_p[k] = x[k*dim + 2]

            # if(out_put):

            #     out.write( ','.join([str(k), str(x[k*dim][0]), str(x[k*dim +1 ][0]), str(x[k*dim +2][0]), str(t[f]) ])+'\n')

                
            # ########## plot position x vs position y
            # plt.clf()
            # plt.scatter(x_p, y_p)
            # plt.xlim(0., 3.)
            # plt.ylim(0., 2.)
            # plt.xlabel("x")
            # plt.ylabel("y") 
            # plt.show()
            # plt.pause(0.001)

    out.close()
    ########### plot energy  
    # plot_energy(v, springs, d_reduct_init, dt, mass_matrix)
    # ########## plot position vs time
    plt.plot(t, snapshot_positions[0], 'r--', t, snapshot_positions[3], 'b-.', t, snapshot_positions[6], 'g--', t, snapshot_positions[9], 'b--' )
    plt.ylabel("position")
    plt.xlabel("time")
    plt.show()
    plt.pause(100)


        
                   

if __name__ == "__main__":

    start = time.time()
    main()
    end = time.time()
    print " ############# Run Time: ",end - start
