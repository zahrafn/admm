#! /usr/bin/env python
import numpy as np
import math
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from math import sqrt, log
from numpy import linalg as LA
from itertools import combinations
from scipy.optimize import minimize
import time


class Spring:

    def __init__(self, s1, s2,inx, id1, id2, n, dim, stiffness):

        # the spring has always 2 masses holding each side of it
        self.s1 = s1
        self.s2 = s2
        self.id1 = id1
        self.id2 = id2
        self.inx = inx
        self.n =n
        self.dim = dim
        self.x = np.array([s1,s2])
        self.stiffness = stiffness
        self.weight = np.sqrt(self.stiffness)
        dist_v = self.s1-self.s2
        self.rest_dist = LA.norm(dist_v)

        
    
    def get_reduction_matrix(self, d_reduct):

        i_m = np.identity(self.dim)
        d_reduct[self.inx * self.dim : self.inx* self.dim + self.dim, self.id1*self.dim:self.id1*self.dim+self.dim] = -1 * i_m
        d_reduct[self.inx * self.dim : self.inx* self.dim + self.dim, self.id2*self.dim:self.id2*self.dim+self.dim] = i_m
        
        return d_reduct


#############################################################################################################################

def energy( xy_m, args):
    
    inx, x, x_tilde, springs, d_reduct_init, dt, mass_matrix = args
    
    x[inx] = xy_m[0]
    x[inx+1] = xy_m[1]
    x[inx+2] = xy_m[2]

    momentum_part = (1/(np.power(dt,2)) )* np.power(LA.norm( np.dot (np.power(mass_matrix,0.5) , (x - x_tilde ))),2)
    sum_spring_part =0.
    
    for s in springs: 
        
        d_reduct_init = 0.* d_reduct_init
        d_reduct_i = s.get_reduction_matrix(d_reduct_init)
        spring_part_i = s.stiffness * np.power( (LA.norm( np.dot(d_reduct_i, x) ) - s.rest_dist), 2)
        sum_spring_part = sum_spring_part  + spring_part_i
    

    u = 0.5* (momentum_part + sum_spring_part)

    return u


def plot_energy(v, springs, d_reduct_init, dt, mass_matrix):
    # ### ploting the energy 
    x1_p = [x for x in range(1,50)]
    x_p = [0, 0]
    u_p=[]

    for i in range(len(x1_p)):
        x_p[0] = x1_p[i]
        x_tilde_p = x_p + v * dt
        args_p = [0, x_p, x_tilde_p, springs, d_reduct_init, dt, mass_matrix]
        u_p_e = energy(x_p[0],args_p)
        u_p = np.append(u_p,u_p_e)

    plt.plot(x1_p,u_p)
    plt.ylabel("energy")
    plt.xlabel("position particle 1")
    plt.show()
    plt.pause(20)


def config_reader(filename ):
    x =[]
    v = []
    config=open( filename , 'r')
    first_line = config.readline()

    for line in config:

        line=line.split(",")
        if(line[0] != 'sets'):

            x.append(np.float(line[1]))
            x.append(np.float(line[2]))
            x.append(np.float(line[3]))
            v.append(np.float(line[4]))
            v.append(np.float(line[5]))
            v.append(np.float(line[6]))

        else:
            sets = np.array(line[1:])


    return x,v, sets





def main():

    ################# initialization #######################
    visualization = True
    out_put = True
    dim = 3
    filename = 'config.txt'

    ################# parameters 
    number_frames = 400
    dt = 0.005
    init_mass = 1.
    stiffness = 10.
    ##########################################################

    ############# read position and velocity of particles from config file
    x,v, sets = config_reader(filename )
    x = np.array(x)
    v = np.array(v)

    number_particles = len(x)/ dim
    number_springs = len(sets)

    
    ################### making springs list

    springs =[]
    for i, s in enumerate(sets):

        s = s.strip()
        inx_1 = int(s[0])
        inx_2 = int(s[1])
        sp = Spring(x[inx_1 * dim : inx_1* dim + dim], x[inx_2 * dim : inx_2* dim + dim], i ,inx_1 , inx_2, number_particles ,dim, stiffness )
        springs = np.append(springs,sp)
    
    ################### making mass matrix
    particle_mass = np.zeros(len(x))
    for i in range(number_particles):

        j = dim*i
        particle_mass[j] = init_mass
        particle_mass[j+1] = init_mass
        particle_mass[j+2] = init_mass
        
    mass_matrix = np.identity(len(x)) * particle_mass
    

    #going over all particles one by one 
    x = x.reshape((len(x), 1))
    v = v.reshape((len(v), 1))
    x_tilde = x
        
    
    print "################### simulation starts ###################"
    plt.ion()

    t = []
    t_all =0

    x_p = np.zeros(number_particles)
    y_p = np.zeros(number_particles) 
    z_p = np.zeros(number_particles) 


        
    d_reduct_init =  np.zeros((number_springs *dim ,number_particles*dim))
    old_x = np.empty_like(x)

    if(out_put):
        out=open('gs_posiotns_time.txt','w')
        out.write("id, x, y, z, time"+'\n')
        
        
    for f in range(number_frames):

        t = np.append(t,t_all)   

        print "################### frame: ", f
        x_tilde = x + v * dt
        old_x[:] = x[:]
        # print "v: \n", v
        #while (error > target_error):
            
        # filling out the new optimized x
        energy_sum =0

        for i in range(number_particles):

            if(out_put):

                out.write( ','.join([str(i), str(x[i*dim][0]), str(x[i*dim +1 ][0]), str(x[i*dim +2][0]), str(t[f]) ])+'\n')       


                # print "########## paritcle ", i
                ### plot positions

            args = [i*dim, x, x_tilde, springs, d_reduct_init, dt, mass_matrix]
            x_m = np.array([x[i*dim],x[i*dim+1], x[i*dim+2]])
            x_min_i = minimize(energy, x_m , args = args, jac=False )
                

            x[i*dim] = x_min_i.x[0]
            x[i*dim +1] = x_min_i.x[1]
            x[i*dim +2] = x_min_i.x[2]

            # x_p[i] = x[i*dim]
            # y_p[i] = x[i*dim + 1]
            # z_p[i] = x[i*dim + 2]
     
            v = (x - old_x)/dt

            ########## plot position x vs position y
            # plt.clf()
            # plt.scatter(x_p, y_p)
            # plt.xlim(0., 3.)
            # plt.ylim(0., 2.)
            # plt.xlabel("x")
            # plt.ylabel("y") 
            # plt.show()
            # plt.pause(0.001)
    
        t_all = t_all+dt 

        if(f==0):
            snapshot_positions = x
        else:
            snapshot_positions =np.hstack(( snapshot_positions, x))
    
    ########### plot energy  
    # plot_energy(v, springs, d_reduct_init, dt, mass_matrix)
    # ########## plot position vs time
    plt.plot(t, snapshot_positions[0], 'r--', t, snapshot_positions[3], 'b-.', t, snapshot_positions[6], 'g--', t, snapshot_positions[9], 'b--' )
    plt.ylabel("position")
    plt.xlabel("time")
    plt.show()
    plt.pause(100)



        
                   

if __name__ == "__main__":

    start = time.time()
    main()
    end = time.time()
    print " ############# Run Time: ",end - start
