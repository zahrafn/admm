#! /usr/bin/env python
import numpy as np
import matplotlib.pyplot as plt

data_admm=open('admm_posiotns_time.txt','r')
data_a = data_admm.read()
data_ad = data_a.split("\n")


# making lists to put data in it
t= []
g_x1=[]
g_x2= []

# going throw each line and read each element
for line in data_ad:
  line = line.split(',')
  #print line[0]
  t.append(np.float(line[0]))
  g_x1.append(np.float(line[1]))
  g_x2.append(np.float(line[2]))

data_admm.close()


data_gauss=open('gauss_posiotns_time.txt','r')
data_g = data_gauss.read()
data_gs = data_g.split("\n")


# making lists to put data in it
t_a= []
a_x1=[]
a_x2= []

# going throw each line and read each element
for line in data_gs:
  line = line.split(',')
  t_a.append(np.float(line[0]))
  a_x1.append(np.float(line[1]))
  a_x2.append(np.float(line[2]))

data_gauss.close()


plt.plot(t_a, a_x1, 'r', t_a, a_x2, 'b', label = 'admm' )
plt.plot(t, g_x1, 'r-.', t, g_x2, 'b-.', label ='gauss')
plt.legend()
plt.ylabel("position")
plt.xlabel("time")
plt.show()


