import numpy as np
import math
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from math import sqrt, log
from numpy import linalg as LA
from itertools import combinations
import time


class Spring:

    def __init__(self, s1, s2,inx, id1, id2, n, dim, stiffness):

        # the spring has always 2 masses holding each side of it
        self.s1 = s1
        self.s2 = s2
        self.id1 = id1
        self.id2 = id2
        self.inx = inx
        self.n =n
        self.dim = dim
        self.x = np.array([s1,s2])
        self.stiffness = stiffness
        self.weight = np.sqrt(self.stiffness) 
        dist_v = self.s1-self.s2
        self.rest_dist = LA.norm(dist_v)

    
    def get_reduction_matrix(self, d_reduct):
    
        i_m = np.identity(self.dim)
        d_reduct[self.inx * self.dim : self.inx* self.dim + self.dim, self.id1*self.dim:self.id1*self.dim+self.dim] = -1 * i_m
        d_reduct[self.inx * self.dim : self.inx* self.dim + self.dim, self.id2*self.dim:self.id2*self.dim+self.dim] = i_m

        return d_reduct
    

    def update_zu(self,x_i,u_i,z_i,d_reduct_i ):

        dxplu = np.dot(d_reduct_i, x_i) + u_i
        p = dxplu/LA.norm(dxplu) * self.rest_dist
        z_i = ( 1.0 / (self.weight * self.weight + self.stiffness) ) * (self.stiffness*p + self.weight * self.weight*(dxplu))
        u_i = u_i + np.dot(d_reduct_i, x_i) - z_i

        return (z_i, u_i)

#############################################################################################################################


def update_x(x_tilde, z , u, d_reduct, dt, mass_matrix, weight_matrix, roth ):

    wTw = np.dot(np.transpose(weight_matrix),weight_matrix)
    dTwTw = np.dot(np.transpose(d_reduct), wTw)
    dTwTwd = np.dot(dTwTw,d_reduct )
    xn_p1 = LA.inv(np.add(mass_matrix, roth*dt*dt* dTwTwd))
    xn_p2 = np.add(np.dot(mass_matrix,x_tilde), roth*dt*dt*np.dot(dTwTw,z- u))
    xn = np.dot(xn_p1,xn_p2 )
    # print "wTw: \n", wTw
    # print "d_reduct \n", np.transpose(d_reduct)

    return xn


def optimize_x(x_tilde, z, u, springs, d_reduct, dt, mass_matrix, weight_matrix, roth, dim, nIter ):

    ############# loop over number of iteration ######################
    # print "weight_matrix \n", weight_matrix
    # print "d_reduct \n", d_reduct
    x = x_tilde
    #print "before x \n", x

    for j in range(nIter):

        # print "Global d_reduct: \n",d_reduct
        
        x = update_x(x_tilde, z , u, d_reduct, dt, mass_matrix, weight_matrix, roth )
        
        for i,s in enumerate(springs):

            # print "########### spring ",i
            #d_reduct_holder = 0.* d_reduct
            #d_reduct_i = s.get_reduction_matrix(d_reduct_holder)
            d_reduct_i = d_reduct[ i*dim:i*dim+dim , i*dim : i*dim+2*dim]
            u_i = u[i*dim : i*dim+dim]
            z_i = z[i*dim : i*dim+dim]
            x_i = x[i*dim : i*dim+2*dim]
            z_i,u_i = s.update_zu(x_i,z_i, u_i, d_reduct_i)

            u[i*dim : i*dim+dim] = u_i
            z[i*dim : i*dim+dim] = z_i
            x[i*dim : i*dim+2*dim] = x_i

            # print "z \n", z
            # print "u \n", u
    
    #print "after x \n", x
    return x

####################################
################## Helper functions

def nCr(n,r):

    f = math.factorial

    return f(n) / f(r) / f(n-r)


def particle_positions(number_of_particles):

    radius = np.random.uniform(0.0,1.0, (number_of_particles,1))
    theta = np.random.uniform(0.,1.,(number_of_particles,1))*np.pi 
    phi = np.arccos(1-2*np.random.uniform(0.0,1.,(number_of_particles,1)))
    x = radius * np.sin( theta ) * np.cos( phi )
    y = radius * np.sin( theta ) * np.sin( phi )
    
    return (x,y)


def config_reader(filename ):
    x =[]
    v = []
    config=open( filename , 'r')
    first_line = config.readline()

    for line in config:

        line=line.split(",")
        if(line[0] != 'sets'):

            x.append(np.float(line[1]))
            x.append(np.float(line[2]))
            x.append(np.float(line[3]))
            v.append(np.float(line[4]))
            v.append(np.float(line[5]))
            v.append(np.float(line[6]))

        else:

            sets = np.array(line[1:])

    return x,v, sets





def main():

    ################# initialization #######################
    visualization = True
    out_put = True
    dim = 3
    filename = 'config.txt'

    ################# parameters 
    number_frames = 400
    nIter =1
    dt = 0.005
    roth = 1.
    init_mass = 1.
    stiffness = 10.
    ##########################################################

    ############# read position and velocity of particles from config file
    x,v, sets = config_reader(filename )
    x = np.array(x)
    v = np.array(v)

    number_particles = len(x)/ dim
    number_springs = len(sets)
    springs =[]
    

    ###### finding all combination of springs 
    ###### making springs for each unique pairs
    l = [i for i in range(len(x))]
    combin =[ comb for comb in combinations(l, 2)]
    ##############################################
    
    # for i,each in enumerate(combin):
    #     ixd_1 = each[0]
    #     ixd_2 = each[1]
    #     s = Spring(x[ixd_1], x[ixd_2], ixd_1, ixd_2, n ,dim, stiffness )
    #     springs = np.append(springs,s)

    ################### making springs list
    for i, s in enumerate(sets):

        s = s.strip()
        inx_1 = int(s[0])
        inx_2 = int(s[1])
        sp = Spring(x[inx_1 * dim : inx_1* dim + dim], x[inx_2 * dim : inx_2* dim + dim], i ,inx_1 , inx_2, number_particles ,dim, stiffness )
        springs = np.append(springs,sp)

    ################### making mass matrix

    particle_mass = np.zeros(len(x))
    for i in range(number_particles):

        j = dim*i
        particle_mass[j] = init_mass
        particle_mass[j+1] = init_mass
        particle_mass[j+2] = init_mass
        
    mass_matrix = np.identity(len(x)) * particle_mass

    #########################################

    d_reduct_init =  np.zeros((number_springs *dim ,number_particles*dim))

    for i,s in enumerate(springs):

        d_reduct = s.get_reduction_matrix(d_reduct_init)
        # print "d_reduct \n", d_reduct


        
    ################### making stuff me need ###################"
    x = x.reshape((len(x), 1))
    v = v.reshape((len(v), 1))
    z = np.dot(d_reduct, x)
    u_init = np.zeros((len(z),1))


    ################### constructing weight_matrix
    weight_matrix = np.identity(z.shape[0]) 

    for i in range(len(springs)):
        weight_matrix[i*dim][i*dim] = springs[i].weight
        weight_matrix[i*dim+1][i*dim+1] = springs[i].weight
        weight_matrix[i*dim+2][i*dim+2] = springs[i].weight

    weight_matrix_inv = LA.inv(weight_matrix)
    u = u_init
    d_reduct_transpose = np.transpose(d_reduct)
    
#   print "################### initialization ###################"
    wTw = np.dot(np.transpose(weight_matrix),weight_matrix)
    dTwTw = np.dot(np.transpose(d_reduct), wTw)
    dTwTwd = np.dot(dTwTw,d_reduct )
    
    ############ start simulation #####################
    
        
    print "################### simulation starts ###################"
    plt.ion()

    x_tilde = x
    t = []
    t_all = 0


    # x_p = np.zeros(number_particles)
    # y_p = np.zeros(number_particles) 
    # z_p = np.zeros(number_particles) 


    snapshot_positions = [] 


    if(out_put):

        out=open('admm_posiotns_time.txt','w')
        out.write("id, x, y, z, time"+'\n')

    for i in range(number_frames):

        t = np.append(t,t_all)

        if(out_put):
            for k in range(number_particles):
                out.write( ','.join([str(k), str(x[k*dim][0]), str(x[k*dim +1 ][0]), str(x[k*dim +2][0]), str(t[i]) ])+'\n')



        print "################### iteration: ", i
        x_tilde = x+ v *dt

        next_x = optimize_x(x_tilde, z, u, springs, d_reduct, dt, mass_matrix, weight_matrix, roth, dim, nIter )
        v = (next_x - x )/dt
            # print "x: \n", x
        x = next_x



         # plot pos vs time
        t_all = t_all+dt

        if(i==0):
            snapshot_positions = x
        else:
            snapshot_positions =np.hstack(( snapshot_positions, x))

            ########## plot position x vs position y
        # for k in range(number_particles):


        #         x_p[k] = x[k*dim]
        #         y_p[k] = x[k*dim + 1]
        #         z_p[k] = x[k*dim + 2]

            # plt.clf()
            # plt.scatter(x_p, y_p)
            # plt.xlim(0., 3.)
            # plt.ylim(0., 2.)
            # plt.xlabel("x")
            # plt.ylabel("y") 
            # plt.show()
            # plt.pause(0.001)

     

    plt.plot(t, snapshot_positions[0], 'r--', t, snapshot_positions[3], 'b-.', t, snapshot_positions[6], 'g--', t, snapshot_positions[9], 'b--' )
    plt.ylabel("position")
    plt.xlabel("time")
    plt.show()
    plt.pause(100)


if __name__ == "__main__":
    start = time.time()
    main()
    end = time.time()
    print " ############# Run Time: ",end - start      
